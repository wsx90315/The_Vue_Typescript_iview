import App from './App.vue';

import iView from 'iview';

import 'iview/dist/styles/iview.css';

import router from './router';

import Vue from 'vue';
Vue.use(iView);
Vue.config.productionTip = false;
const v = new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
});
