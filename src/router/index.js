import Vue from 'vue'
import Router from 'vue-router'
const main = () => import('@/components/main.vue')
//const Theli1 = () => import('@/components/Theli1')
//const Theli2 = () => import('@/components/Theli2')
//const Theli3 = () => import('@/components/Theli3')


Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component:  main,
      //children:[
      //  { path: 'Theli1' , name:'Theli1' , component: Theli1 },
      //  { path: 'Theli2' , name:'Theli2' , component: Theli2 },
      //  { path: 'Theli3' , name:'Theli3' , component: Theli3 },
      //] 
      
    }
  ]
})
