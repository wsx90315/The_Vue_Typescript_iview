import http from './reddit'

const url = '/api'

export const Getauto = (params) =>{
    return http.Get( url + params)
}
export const TheDelete = (data, params) =>{
    return http.Delete(url + data)
}
export const ThePost = (data, params) =>{
    return http.Put(url + data, params)
}